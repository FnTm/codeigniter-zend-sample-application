<?php
/**
 * User: Janis
 * Date: 11.3.12
 * Time: 22:33
 */
foreach ($articles as $article) {
    ?>

        <p>

            <h2><?php echo htmlspecialchars($article->title);?></h2>
    <div class="sub_title"><?php echo htmlspecialchars($article->cat_title);?> / <a href="<?php echo site_url(array('welcome','edit',$article->id));?>">Rediģēt</a></div>
            <?php echo nl2br(htmlspecialchars($article->contents));?>
        </p>

<?

}

<?php
class Category extends CI_Model{
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    private function _setUpSelect(){
        $this->db->select('*');
        $this->db->from('category');

    }
    public function getAll(){
        $this->_setUpSelect();
       return $this->db->get()->result();
    }
    public function getForSelect(){
        $data=array();
        foreach($this->getAll() as $item){
            $data[$item->id]=$item->title;
        }
        return $data;
    }
    
}
<?php
class Blog extends CI_Model
{
    private $validValues = array('title', 'contents', 'category_id');

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    private function _setUpSelect()
    {
        $this->db->select('blog.*,category.title as cat_title,category.id as cat_id');
        $this->db->from('blog');
        $this->db->join('category', 'blog.category_id=category.id');
    }

    public function getOne($id)
    {
        $this->_setUpSelect();
        $this->db->where('blog.id', $id);
        $return = $this->db->get()->result();
        if (count($return) == "1") {
            return $return[0];
        }
        return false;
    }

    public function getAll()
    {
        $this->_setUpSelect();
        $this->db->order_by('blog.id', 'desc');
        return $this->db->get();
    }

    public function getSelected($id)
    {
        $this->_setUpSelect();
        $this->db->where('blog.id', $id);
        $data = $this->db->get()->result();
        if (count($data) == 1) {
            return $data[0]->id;
        }
        return NULL;

    }

    public function create($data)
    {
        $data = $this->prepData($data);
        $this->db->insert('blog', $data);
    }

    public function updateOne($id, $data)
    {
        $data = $this->prepData($data);
        $this->db->where('id', $id);
        $this->db->update('blog', $data);
    }

    private function prepData($data)
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $this->validValues)) {
                unset($data[$key]);
            }

        }
        return $data;
    }
}
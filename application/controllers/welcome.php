<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH."/core/My_Controller.php");
class Welcome extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    /** @var Blog */
    protected $blogModel;
    /** @var Category */
    protected $catModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Blog');
        $this->load->model('Category');
        $this->blogModel = $this->Blog;
        $this->catModel = $this->Category;
    }

    public function index()
    {
        $data['articles'] = $this->blogModel->getAll()->result();

        $this->data['content'] = $this->load->view('index', $data, true);
        //var_dump($blogModel->getAll());
        $this->load->view('layout', $this->data);
    }

    public function edit($id = NULL)
    {
        if (strlen(intval($id)) == 0 || intval($id) == 0) {
            $this->data['content'] = $this->getView('error');
        }
        else {
            $form = $this->getEditForm($this->catModel->getForSelect());
            if (false !== $this->input->post()) {
                if (true === $form->isValid($this->input->post())) {
                    $this->blogModel->updateOne($id, $this->input->post());
                    redirect('welcome/index', 'location', 302);
                }
                else {
                    $form->populate($this->input->post());

                }
            }
            else {
                $item = $this->blogModel->getOne($id);
                $data['title'] = $item->title;
                $data['contents'] = $item->contents;
                $data['category_id'] = $this->blogModel->getSelected($id);
                $form->populate($data);
            }
            $data = array();


            $data['form'] = $form;
            $this->data['content'] = $this->getView('edit', $data);
        }
        $this->load->view('layout', $this->data);

    }

    public function newArticle()
    {
        $form = $this->getEditForm($this->catModel->getForSelect());
        if (false !== $this->input->post()) {
            if (true === $form->isValid($this->input->post())) {


                $this->blogModel->create($form->getValidValues($this->input->post()));
                redirect('welcome/index', 'location', 302);
            }
            else {
                $form->populate($this->input->post());

            }
        }

        $data['form'] = $form;
        $this->data['content'] = $this->getView('edit', $data);

        $this->load->view('layout', $this->data);
    }


    private function getView($name, $data = array())
    {
        return $this->load->view($name, $data, true);
    }

    private function getEditForm($categories = array())
    {
        $this->zend->load('Zend/Form');
        $this->zend->load('Zend/View');
        $view = new Zend_View();
        require_once(APPPATH . 'forms/editForm.php');
        $form = new editForm($categories);
        return $form->setView($view);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
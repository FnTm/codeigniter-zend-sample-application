<?php
/**
 * User: Janis
 * Date: 11.4.12
 * Time: 18:55
 */

class editForm extends Zend_Form
{
    public function __construct($categories = array())
    {
        parent::__construct();
        $this->setAction(current_url());
        $this->addElement($this->createElement('text', 'title')->setLabel('Virsraksts')->setRequired(true));
        $this->addElement($this->createElement('textarea', 'contents')->setLabel('Saturs')->setRequired(true));
        $cat = $this->createElement('select', 'category_id')->setLabel('Kategorija');
        $cat->setMultiOptions($categories);
        $this->addElement($cat);
        $this->addElement('submit', "submit");
    }
}

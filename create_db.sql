SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

CREATE DATABASE `coolblog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `coolblog`;

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_latvian_ci NOT NULL,
  `contents` text COLLATE utf8_latvian_ci NOT NULL,
  `category_id` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci AUTO_INCREMENT=3 ;

INSERT INTO `blog` (`id`, `title`, `contents`, `category_id`) VALUES
(1, 'Jaunumi programmēšanā', 'Šodien neko jaunu par programmēšanu neesmu apguvis :(\r\nVienīgi šo komandu\r\n<script>alert("Hei, javaScript injekcija!")</script>', 1),
(2, 'Brīvdienu plāni', 'Šajās brīvdienās neko jēdzīgu neplānoju darīt.', 2);

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_latvian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci AUTO_INCREMENT=3 ;


INSERT INTO `category` (`id`, `title`) VALUES
(1, 'Programmēšana'),
(2, 'Brīvais laiks');


CREATE USER 'bloguser'@'localhost' IDENTIFIED BY  'tt2md2omg';

GRANT USAGE ON * . * TO  'bloguser'@'localhost' 
    IDENTIFIED BY  'tt2md2omg' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;

GRANT ALL PRIVILEGES ON  `coolblog` . * TO  'bloguser'@'localhost' WITH GRANT OPTION ;


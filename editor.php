<?php
$server = 'localhost';
$database = 'coolblog';
$user = 'bloguser';
$password = 'tt2md2omg';

$link = mysql_connect($server, $user, $password);
mysql_set_charset("utf8", $link);
mysql_select_db($database, $link);

//prot nolasīt visas kategorijas no DB
function get_categories() {
    global $link;
    $query = "select * from category";
    $dataset = mysql_query($query, $link);
    $result = array();
    while ($data = mysql_fetch_object($dataset))
	$result[$data->id] = $data->title;
    return $result;
}

$actions = array("new" => "Jauns ieraksts",
    "savenew" => "Jauns ieraksts",
    "edit" => "Ieraksta labošana",
    "saveedit" => "Ieraksta labošana",);

//no GET parametriem būs jāizdomā, kas jādara
if (!isset($_GET["action"]) ||
	($_GET["action"] == "") ||
	($_GET["action"] == "new")) {
    $action = "new";
} elseif (array_key_exists($_GET["action"], $actions)) {
    $action = $_GET["action"];
} else
    die("Tev te nevajadzēja būt!");

$id = 0;
if (isset($_GET["id"]) && intval($_GET["id"]) > 0) {
    $id = intval($_GET["id"]);
} elseif (($action != "new") && ($action != "savenew")) {
    die("Neesi padevis ID parametru!");
}

$categories = get_categories();
$errors = array();
if ($action == "new") {
    //pirmoreiz atvērta forma, veidos jaunu
    $current_category_id = "0";
    $current_title = "";
    $current_contents = "";
    $next_action = "savenew";
} elseif ($action == "savenew") {
    //dati ir ievadīti, var mēģināt saglabāt
    $current_category_id = intval($_POST["category"]);
    if ($current_category_id == 0)
	$errors[] = "Nav norādīta kategorija";

    $current_title = $_POST["title"];
    if (trim($current_title) == "")
	$errors[] = "Nav norādīts virsraksts";

    $current_contents = $_POST["contents"];

    if (count($errors) == 0) {
	$query = 'insert into blog (title, contents, category_id)
		    values (
		     "' . mysql_real_escape_string($current_title) . '",
		     "' . mysql_real_escape_string($current_contents) . '"
		      , ' . $current_category_id . ')';
	mysql_query($query, $link);
	header("location:./");
	exit();
    } else {
	$next_action = "savenew";
    }
} elseif ($action == "edit") {
    //atvērts labošanai - nolasa no DB, ielādē mainīgajos

    $query = "select * from blog where blog.id = " . $id;
    $dataset = mysql_query($query, $link);
    if (mysql_num_rows($dataset) == 0)
	die("Meklēji neesošu ID");
    $blogentry = mysql_fetch_object($dataset);
    $current_category_id = $blogentry->category_id;
    $current_contents = $blogentry->contents;
    $current_title = $blogentry->title;
    $next_action = "saveedit&id=".$id;
} elseif ($action == "saveedit") {
    //dati ir ievadīti, var mēģināt saglabāt
    $current_category_id = intval($_POST["category"]);
    if ($current_category_id == 0)
	$errors[] = "Nav norādīta kategorija";

    $current_title = $_POST["title"];
    if (trim($current_title) == "")
	$errors[] = "Nav norādīts virsraksts";

    $current_contents = $_POST["contents"];
    if (count($errors) == 0) {
	$query = 'update blog set 
		     title= "' . mysql_real_escape_string($current_title) . '",
		     contents="' . mysql_real_escape_string($current_contents) . '",
		     category_id  = ' . $current_category_id . '
		    where id =' . $id;
	mysql_query($query, $link);
	header("location:./");
	exit();
    } else {
	$next_action = "saveedit&id=".$id;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
	<h1><?php echo $actions[$action]; ?></h1>
	<?php
	if (count($errors) > 0) {
	    echo "<p>" . implode($errors, "<br />") . "</p>";
	}
	?>
	<form action="?action=<?php echo $next_action; ?>" method="POST">
	    <fieldset>
		<p><label for="title">Nosaukums</label><br />
		    <input type="text" name="title" id="title" value="<?php
	echo htmlspecialchars($current_title);
	?>"/>
		</p>

		<p><label for="contents">Teksts</label><br />
		    <textarea name="contents" id="contents" cols="60" rows="8"><?php
			   echo htmlspecialchars($current_contents);
	?></textarea>
		</p>

		<p><label for="category">Kategorija</label><br />
		    <select name="category" id="category">
			<option value="0" <?php
			if ($current_category_id == 0) {
			    echo "selected='selected'";
			}
	?>>Izvēlies kategoriju</option>
				<?php
				foreach ($categories as $id => $title) {
				    echo '<option value="' . $id . '"';
				    if ($id == $current_category_id) {
					echo " selected='selected'";
				    }
				    echo '>' . htmlspecialchars($title) . "</option>";
				}
				?>
		    </select>
		</p>
		<p><input type="submit" value="Saglabāt" /></p>

	    </fieldset>
	</form>

    </body>
</html>
